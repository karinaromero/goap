﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject patientPrefab;
    public int numPatients;

    void Start()
    {
        for (int i = 0; i < numPatients; i++)
        {
            var clone = Instantiate(patientPrefab, this.transform.position, Quaternion.identity);
            clone.transform.parent = this.gameObject.transform;

        }

        Invoke("SpawnPatient", 7);
    }

    void SpawnPatient()
    {
        var clone = Instantiate(patientPrefab, this.transform.position, Quaternion.identity);

        clone.transform.parent = this.gameObject.transform;
        Invoke("SpawnPatient", Random.Range(9, 25));
    }

    void Update()
    {
        
    }
}
