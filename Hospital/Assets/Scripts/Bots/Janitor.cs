﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Janitor : GAgent
{
    string[] statesToInvoke = { "exhausted", "bathroom" };

    void Start()
    {
        base.Start();
        SubGoal subGoal = new SubGoal("cleaned", 1, false);
        goals.Add(subGoal, 1);

       /* SubGoal subGoal2 = new SubGoal("rested", 1, false);
        goals.Add(subGoal2, 3);

        SubGoal subGoal3 = new SubGoal("ready", 1, false);
        goals.Add(subGoal3, 2);

        Invoke("GetTired", Random.Range(10, 20));*/
    }

    /*void GetTired()
    {
        beliefs.ModifyState("exhausted", 0);
        Invoke("GetTired", Random.Range(10,20));
    }*/
    void GetTired()
    {
        int index = Random.Range(1, 3);
        beliefs.ModifyState(statesToInvoke[index - 1], 0);
        Invoke("GetTired", Random.Range(6, 10));
    }
}

