﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doctor : GAgent
{
    string[] statesToInvoke = { "exhausted", "bathroom" };

    void Start()
    {
        base.Start();
        
        SubGoal subGoal = new SubGoal("research", 1, false);
        goals.Add(subGoal, 1);
        
        SubGoal subGoal1 = new SubGoal("rested", 1, false);
        goals.Add(subGoal1, 3);

        SubGoal subGoal2 = new SubGoal("ready", 1, false);
        goals.Add(subGoal2, 3);

        Invoke("GetTired", Random.Range(6, 10));
    }

    void GetTired()
    {
        int index = Random.Range(1, 3);
        beliefs.ModifyState(statesToInvoke[index-1], 0);
        Invoke("GetTired", Random.Range(6, 10));
    }

}
