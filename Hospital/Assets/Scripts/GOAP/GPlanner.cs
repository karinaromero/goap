﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public Node parent;
    public float cost;
    public Dictionary<string, int> states;
    public GAction action;

    public Node(Node parent, float cost, Dictionary<string, int> states, GAction action)
    {
        this.parent = parent;
        this.cost = cost;
        this.states = new Dictionary<string, int>(states);
        this.action = action;
    }
    public Node(Node parent, float cost, Dictionary<string, int> states, Dictionary<string, int> beliefStates, GAction action)
    {
        this.parent = parent;
        this.cost = cost;
        this.states = new Dictionary<string, int>(states);

        foreach (KeyValuePair<string, int> bs in beliefStates)
        {
            if (!this.states.ContainsKey(bs.Key))
            {
                this.states.Add(bs.Key, bs.Value);
            }
        }

        this.action = action;
    }
}

public class GPlanner 
{
    public Queue<GAction> Plan(List<GAction> actions, Dictionary<string, int> goal, WorldStates beliefStates)
    {
        List<GAction> usableActions = new List<GAction>();

        foreach (GAction a in actions)
        {
            if (a.IsAchievable())
            {
                usableActions.Add(a);
            }
        }

        List<Node> leaves = new List<Node>();
        Node start = new Node(null, 0, GWorld.Instance.GetWorldStates().GetStates(), beliefStates.GetStates(), null);

        bool success = BuildGraph(start, leaves, usableActions, goal);

        if (!success)
        {
            //Debug.Log("No plan");
            return null;
        }

        Node cheapest = null;

        foreach (Node leaf in leaves)
        {
            if (cheapest == null)
            {
                cheapest = leaf;
            }
            else
            {
                if(leaf.cost < cheapest.cost)
                {
                    cheapest = leaf;
                }
            }
        }

        List<GAction> result = new List<GAction>();
        Node n = cheapest;

        while (n != null)
        {
            if(n.action != null)
            {
                result.Insert(0, n.action);
            }
            n = n.parent;
        }
        Queue<GAction> queActions = new Queue<GAction>();

        foreach (GAction a in result)
        {
            queActions.Enqueue(a);
        }
        Debug.Log("The plan is : ");

        foreach (GAction a in queActions)
        {
            Debug.Log("Q: " + a.actionName);
        }

        return queActions;
    }

    public bool BuildGraph(Node parent, List<Node> leaves, List<GAction> usableActions, Dictionary<string, int> goal)
    {
        bool foundPath = false;

        foreach (GAction action in usableActions)
        {
            if (action.IsAchievableGiven(parent.states))
            {
                Dictionary<string, int> currentState = new Dictionary<string, int>(parent.states);

                foreach (KeyValuePair<string, int>  eff in action.effects)
                {
                    if (!currentState.ContainsKey(eff.Key))
                    {
                        currentState.Add(eff.Key, eff.Value);
                    }
                }
                Node node = new Node(parent, parent.cost + action.cost, currentState, action);

                if (GoalArchived(goal, currentState))
                {
                    leaves.Add(node);
                    foundPath = true;
                }
                else
                {
                    List<GAction> subset = ActionSubset(usableActions, action);

                    bool found = BuildGraph(node, leaves, subset, goal);

                    if (found)
                    {
                        foundPath = true;
                    }
                }
            }
        }
        return foundPath;
    }

    private bool GoalArchived(Dictionary<string, int> goal, Dictionary<string, int> currentState)
    {
        foreach (KeyValuePair<string, int> g in goal)
        {
            if (!currentState.ContainsKey(g.Key))
            {
                return false;
            }
        }
        return true;
    }

    private List<GAction> ActionSubset(List<GAction> usableActions, GAction actionToRemove)
    {
        List<GAction> subset = new List<GAction>();

        foreach (GAction a in usableActions)
        {
            if (!a.Equals(actionToRemove))
            {
                subset.Add(a);
            }
        }
        return subset;
    }
}
