﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WInterface : MonoBehaviour
{
    GameObject focusObject;
    Vector3 goalPos;

    public GameObject newResourcePrefab;
    public GameObject parent;
    public NavMeshSurface surface;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (!Physics.Raycast(ray, out hit))
            {
                return;
            }
            goalPos = hit.point;
            focusObject = Instantiate(newResourcePrefab, goalPos, newResourcePrefab.transform.rotation);
            //focusObject.transform.parent = parent.transform;
        }
        else if (focusObject && Input.GetMouseButtonUp(0))
        {
            focusObject.transform.parent = parent.transform;
            surface.BuildNavMesh();
            GWorld.Instance.GetQueue("bathrooms").AddResource(focusObject);
            GWorld.Instance.GetWorldStates().ModifyState("FreeBathroom", 1);
            focusObject = null;
        }
        else if (focusObject && Input.GetMouseButton(0))
        {
            RaycastHit hitMove;
            Ray rayMove = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (!Physics.Raycast(rayMove, out hitMove))
            {
                return;
            }

            goalPos = hitMove.point;
            focusObject.transform.position = goalPos;
        }
    }
}
