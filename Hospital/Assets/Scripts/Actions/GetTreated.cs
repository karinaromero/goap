﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetTreated : GAction
{
    public override bool PrePerform()
    {
        //Debug.LogWarning("Calling to PrePerform " + this.actionName);

        target = inventory.FindItemWithTag("Cubicle");

        if (target == null)
        {
            return false;
        }

        return true;
    }
    public override bool PostPerform()
    {
        //Debug.LogWarning("Calling to PostPerform " + this.actionName);
        GWorld.Instance.GetWorldStates().ModifyState("Treated", 1);
        beliefs.ModifyState("isCured", 1);
        inventory.RemoveItem(target);
        return true;
    }
}

