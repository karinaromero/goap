﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SubGoal
{
    public Dictionary<string, int> subGals;
    public bool remove;

    public SubGoal(string s, int i, bool r)
    {
        subGals = new Dictionary<string, int>();
        subGals.Add(s, i);
        remove = r;
    }
}

public class GAgent : MonoBehaviour
{
    public List<GAction> actions = new List<GAction>();
    public GAction currentAction;
    public WorldStates beliefs = new WorldStates();

    public GInventory inventory = new GInventory();

    Queue<GAction> actionQueue;

    public Dictionary<SubGoal, int> goals = new Dictionary<SubGoal, int>(); 
    SubGoal currentGoal;

    GPlanner planner;
    bool invoked = false;

    // Start is called before the first frame update
    public void Start()
    {
        GAction[] acts = this.GetComponents<GAction>();

        foreach (GAction a in acts)
        {
            actions.Add(a);
        }
    }

    public void LateUpdate()
    {
        if (currentAction != null && currentAction.running)
        {
            float distanceToTarget = Vector3.Distance(currentAction.target.transform.position, this.transform.position);
            if (/*currentAction.navMeshAgent.hasPath &&*/ distanceToTarget < 2f)//currentAction.agent.remainingDistance < 0.5f)
            {
                if (!invoked)
                {
                    Invoke("CompleteAction", currentAction.duration);
                    invoked = true;
                }
            }
            return;
        }

        if (planner == null || actionQueue == null)
        {
            planner = new GPlanner();

            var sortedGoals = from entry in goals orderby entry.Value descending select entry;

            foreach (KeyValuePair<SubGoal, int> sg in sortedGoals)
            {
                actionQueue = planner.Plan(actions, sg.Key.subGals, beliefs);
                if (actionQueue != null)
                {
                    currentGoal = sg.Key;
                    break;
                }
            }
        }

        if (actionQueue != null && actionQueue.Count == 0)
        {
            if (currentGoal.remove)
            {
                /*foreach (KeyValuePair<string, int> sg in currentGoal.subGals)
                {
                    Debug.LogError("Removing goal from: " + this.gameObject.name + "  subgoal: " + sg.Key);
                }*/
                goals.Remove(currentGoal);
            }
            planner = null;
        }

        if (actionQueue != null && actionQueue.Count > 0)
        {
            currentAction = actionQueue.Dequeue();

            if (currentAction.PrePerform())
            {
                if (currentAction.target == null && currentAction.targetTag != "")
                {
                    currentAction.target = GameObject.FindWithTag(currentAction.targetTag);
                }
                if (currentAction.target != null)
                {
                    currentAction.running = true;
                    currentAction.navMeshAgent.SetDestination(currentAction.target.transform.position);
                }
            }
            else
            {
                actionQueue = null;
            }
        }
    }

    public void CompleteAction()
    {
        currentAction.running = false;
        currentAction.PostPerform();
        invoked = false;
    }
}
