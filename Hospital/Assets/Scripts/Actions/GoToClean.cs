﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToClean : GAction
{
    GameObject resource;

    public override bool PrePerform()
    {
        target = GWorld.Instance.GetQueue("puddles").RemoveResource();
        if (target == null)
        {
            return false;
        }
        inventory.AddItem(target);
        GWorld.Instance.GetWorldStates().ModifyState("SpawnPuddle", -1);
        return true;
    }
    public override bool PostPerform()
    {
        inventory.RemoveItem(target);
        if (target)
        {
            Destroy(target);
        }
        return true;
    }
}
