﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToBathroom : GAction
{
    public override bool PrePerform()
    {
        target = GWorld.Instance.GetQueue("bathrooms").RemoveResource();

        if(target == null)
        {
            return false;
        }
        inventory.AddItem(target);
        GWorld.Instance.GetWorldStates().ModifyState("FreeBathroom", -1);
        return true;
    }
    public override bool PostPerform()
    {
        GWorld.Instance.GetQueue("bathrooms").AddResource(target);
        inventory.RemoveItem(target);
        GWorld.Instance.GetWorldStates().ModifyState("FreeBathroom", 1);
        beliefs.RemoveState("bathroom");
        return true;
    }
}