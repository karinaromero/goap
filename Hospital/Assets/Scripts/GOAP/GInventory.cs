﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GInventory
{
    List<GameObject> items = new List<GameObject>();

    public void AddItem(GameObject itemToAdd)
    {
        items.Add(itemToAdd);
    }

    public GameObject FindItemWithTag(string tag)
    {
        foreach (GameObject item in items)
        {
            if (item.tag == tag)
            {
                return item;
            }
        }
        return null;
    }

    public void RemoveItem(GameObject itemToRemove)
    {
        if (items.Contains(itemToRemove))
        {
            items.Remove(itemToRemove);
            Debug.Log("Removing item :: " + itemToRemove.name);
        }

        /*int indexToRemove = -1;
        foreach (GameObject g in items)
        {
            indexToRemove++;
            if (g == itemToRemove)
            {
                break;
            }
        }
        if (indexToRemove >= -1)
        {
            items.RemoveAt(indexToRemove);
        }*/
    }

    public List<GameObject> GetItems()
    {
        return this.items;
    }
}
