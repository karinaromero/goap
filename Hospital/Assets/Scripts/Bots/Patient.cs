﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patient : GAgent
{
    void Start()
    {
        base.Start();
        SubGoal subGoal = new SubGoal("isWaiting", 1, true);
        goals.Add(subGoal, 3);

        SubGoal subGoal2 = new SubGoal("isTreated", 1, true);
        goals.Add(subGoal2, 5);

        SubGoal subGoal3 = new SubGoal("isHome", 1, true);
        goals.Add(subGoal3, 9);

        SubGoal subGoal4 = new SubGoal("ready", 1, false);
        goals.Add(subGoal4, 3);

        Invoke("GetBathroom", Random.Range(6, 10));
    }

    void GetBathroom()
    {
        beliefs.ModifyState("bathroom", 0);
        Invoke("GetTired", Random.Range(10, 20));
    }
}
