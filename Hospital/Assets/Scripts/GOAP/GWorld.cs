﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceQueue
{
    public Queue<GameObject> gameObjectsQueue; /*= new Queue<GameObject>();*/
    public string tag;
    public string state;

    public ResourceQueue()
    {
        tag = "";
        state = "";
        gameObjectsQueue = new Queue<GameObject>();
    }

    public ResourceQueue(string tag, string state, WorldStates worldStates)
    {
        this.tag = tag;
        this.state = state;

        if(this.tag != null)
        {
            gameObjectsQueue = new Queue<GameObject>(GameObject.FindGameObjectsWithTag(this.tag));
        }

        if (gameObjectsQueue.Count > 0 && this.state != "")
        {
            worldStates.ModifyState(this.state, gameObjectsQueue.Count);
        }
    }

    public void AddResource(GameObject prefab)
    {
        gameObjectsQueue.Enqueue(prefab);
    }

    public GameObject RemoveResource()
    {
        if (gameObjectsQueue.Count == 0)
        {
            return null;
        }
        return gameObjectsQueue.Dequeue();
    }
}

public sealed class GWorld
{
    private static readonly GWorld instance = new GWorld();

    private static WorldStates worldStates;

    private static ResourceQueue patients;
    private static ResourceQueue cubicles;
    private static ResourceQueue offices;
    private static ResourceQueue bathrooms;
    private static ResourceQueue puddles;

    private static Dictionary<string, ResourceQueue> resources = new Dictionary<string, ResourceQueue>();

    static GWorld()
    {
        worldStates = new WorldStates();

        patients = new ResourceQueue();

        cubicles = new ResourceQueue("Cubicle", "FreeCubicle", worldStates);
        offices = new ResourceQueue("Office", "FreeOffice", worldStates);
        bathrooms = new ResourceQueue("Bathroom", "FreeBathroom", worldStates);
        puddles = new ResourceQueue("Puddle", "SpawnPuddle", worldStates);

        resources.Add("patients", patients);
        resources.Add("cubicles", cubicles);
        resources.Add("offices", offices);
        resources.Add("bathrooms", bathrooms);
        resources.Add("puddles", puddles);
    }

    private GWorld()
    {

    }

    public ResourceQueue GetQueue(string type)
    {
        return resources[type];
    }

    /*
    public void AddPatient(GameObject patientprefab)
    {
        patients.AddResource(patientprefab);
    }

    public GameObject RemovePatient()
    {
        if (patients.Count == 0)
        {
            return null;
        }
        return patients.Dequeue();
    }

    public void AddCubicle(GameObject cubicleprefab)
    {
        cubicles.Enqueue(cubicleprefab);
    }

    public GameObject RemoveCubicle()
    {
        if (cubicles.Count == 0)
        {
            return null;
        }
        return cubicles.Dequeue();
    }
    public void AddOffice(GameObject officeprefab)
    {
        offices.Enqueue(officeprefab);
    }

    public GameObject RemoveOffice()
    {
        if (offices.Count == 0)
        {
            return null;
        }
        return offices.Dequeue();
    }
    public void AddBathroom(GameObject bathroomprefab)
    {
        bathrooms.Enqueue(bathroomprefab);
    }

    public GameObject RemoveBathroom()
    {
        if (bathrooms.Count == 0)
        {
            return null;
        }
        return bathrooms.Dequeue();
    }*/

    public static GWorld Instance
    {
        get { return instance; }
    }
    public WorldStates GetWorldStates()
    {
        return worldStates;
    }
}
