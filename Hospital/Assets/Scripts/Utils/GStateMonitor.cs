﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GStateMonitor : MonoBehaviour
{
    public string state;
    public float stateStrength;
    public float stateDecadeRate;
    public WorldStates beliefs;
    public GameObject resource;
    public GameObject parent;
    public string queueName;
    public string worldState;
    public GAction action;

    bool stateFound = false;
    float initialStrength;

    private void Awake()
    {
        beliefs = this.GetComponent<GAgent>().beliefs;
        initialStrength = stateStrength;
    }

    private void LateUpdate()
    {
        if (action.running)
        {
            stateFound = false;
            stateStrength = initialStrength;
        }

        if (!stateFound && beliefs.HasState(state))
        {
            stateFound = true;
        }

        if (stateFound)
        {
            stateStrength -= stateDecadeRate * Time.deltaTime;

            if (stateStrength <= 0)
            {
                Vector3 location = new Vector3(transform.position.x, transform.position.y-1, transform.position.z);
                GameObject clone = Instantiate(resource, location, resource.transform.rotation);

                clone.transform.parent = parent.transform;
                
                stateFound = false;
                stateStrength = initialStrength;
                beliefs.RemoveState(state);
                GWorld.Instance.GetQueue(queueName).AddResource(clone);
                GWorld.Instance.GetWorldStates().ModifyState(worldState, 1);
            }
        }
    }
}
